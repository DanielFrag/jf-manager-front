import React from 'react';
import { Spin, message, Row, Col, Input, List } from 'antd';
import CClientCard from './client-data-component';
import Client from '../controllers/client-controller';
import CDebtSummary from './debt-summary-component';
const { Search } = Input;
const client = new Client();
class CHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      previousSearch: null,
      searchAcc: 0
    }
  }
  getClientCard(clientData, selectable) {
    if (selectable) {
      clientData.expandAction = (registrationNumber) => this.getClientData(registrationNumber);
    }
    return <CClientCard {...clientData} />
  }
  async getClientData(searchValue, event) {
    if (event) {
      event.preventDefault();
    }
    let { previousSearch, searchAcc } = this.state;
    if (typeof previousSearch === 'string' && searchValue === previousSearch) {
      searchAcc++;
    } else {
      previousSearch = searchValue;
      searchAcc = 0;
    }
    this.setState({
      loading: true
    });
    const nextState = {
      loading: false,
      clientData: null
    };
    try {
      const result = await client.getClients(searchValue, searchAcc + 1);
      if (!result.data || !result.data.length) {
        throw new Error('Não foi possível encontrar o cliente');
      }
      nextState.clientData = result.data;
    } catch (e) {
      searchAcc = 0;
      previousSearch = '';
      message.error(e.message);
    }
    nextState.searchAcc = searchAcc;
    nextState.previousSearch = previousSearch;
    this.setState(nextState);
  }
  getSpin() {
    return <Spin />;
  }
  getDebtsList(debtsArray) {
    const inDebt = debtsArray.filter(d => !d.paymentId && d.valid);
    if (!inDebt.length) {
      message.success('Este aluno está em dia');
      return null;
    }
    inDebt.sort((a, b) => {
      return a.generated.date < b.generated.date ? -1 : 1;
    });
    return inDebt.map(d => <CDebtSummary key={d._id} debtData={{ ...d }} />);
  }
  buildClientsList(clients) {
    return (
      <div>
        <List
          grid={{
            gutter: 16,
            column: 4
          }}
          dataSource={clients}
          renderItem={item => (
            <List.Item>
              {this.getClientCard(item, true)}
            </List.Item>
          )}
        />
      </div>
    );
  }
  buildDataPanel() {
    let { clientData } = this.state;
    if (!clientData) {
      return null;
    }
    if (Array.isArray(clientData)) {
      if (clientData.length > 1) {
        return this.buildClientsList(clientData);
      }
      clientData = clientData[0];
    }
    return (
      <div>
        <Row gutter={16}>
          <Col span={5}>
            {this.getClientCard(clientData)}
          </Col>
          <Col span={19}>
            {this.getDebtsList(clientData.debts)}
          </Col>
        </Row>
      </div>
    );
  }
  render() {
    const { loading, previousSearch } = this.state;
    if (loading) {
      return this.getSpin();
    }
    return (
      <div>
        <div>
          <Search
            placeholder='Digite a matrícula ou o nome do aluno'
            onSearch={(value, event) => this.getClientData(value, event)}
            style={{
              padding: '5px 0px 5px 0px'
            }}
            defaultValue={ previousSearch || '' }
          />
        </div>
        <br />
        <div>
          {this.buildDataPanel()}
        </div>
      </div>
    );
  }
}
export default CHome;
