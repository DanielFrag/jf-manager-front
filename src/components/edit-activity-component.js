import React from 'react';
import { Button, Spin, message, Row, Col, Divider, Menu, Icon, Layout } from 'antd';
const { SubMenu } = Menu;
const { Sider, Content } = Layout;
class CEditActivity extends React.Component {
  render() {
    return (
      <Layout>
        <Sider>
          <Menu
            onClick={this.handleClick}
            style={{ width: '100%' }}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
          >
            <Menu.Item key="list">Listar</Menu.Item>
            <Menu.Item key="add">Inserir</Menu.Item>
            <Menu.Item key="delete">Remover</Menu.Item>
            <Menu.Item key="edit">Editar</Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Content>
            <div style={{ padding: 24 }}>Content</div>
          </Content>
        </Layout>
      </Layout>
    );
  }
}
export default CEditActivity;


/* <Row gutter={16}>
          <Col span={4}>
            Menu
          </Col>
          <Col span={20}>
            Context
          </Col>
        </Row> */