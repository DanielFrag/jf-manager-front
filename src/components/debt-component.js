import React from 'react';
import { Spin, Input, message, Row, Col, Divider, Button, Form, Select, Modal } from 'antd';
import Client from '../controllers/client-controller';
import Debts from '../controllers/debts-controller'
import CClientCard from './client-data-component';
const { Option } = Select;
const client = new Client();
const debts = new Debts();
const { Search } = Input;
const { confirm } = Modal;
const monthList = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
  'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
class DebtForm extends React.Component {
  confirmData() {
    const { form, onRefresh, clientData } = this.props;
    let hasError;
    form.validateFieldsAndScroll((err) => {
      if (err) {
        hasError = true;
      }
    });
    if (hasError) {
      return;
    }
    const formFields = form.getFieldsValue();
    confirm({
      title: `Confirmar a mensalidade da atividade ${formFields.activity}`,
      content: `Ano: ${formFields.year}. Mês: ${formFields.month}. Aluno: ${clientData.name}`,
      async onOk() {
        try {
          await debts.create(clientData, formFields);
          message.success('Atualizado com sucesso');
          form.resetFields();
          onRefresh();
        } catch (e) {
          message.error(e.message);
        }
      },
      onCancel() { }
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    this.confirmData();
  }
  render() {
    const { clientData, form } = this.props;
    const { getFieldDecorator } = form;
    const currentYear = new Date().getFullYear();
    return (
      <Form
        layout='vertical'
        onSubmit={(e) => this.handleSubmit(e)}
      >
        <Form.Item
          label='Atividade'
        >
          {getFieldDecorator('activity', {
            rules: [{
              required: true,
              message: 'Selecione a atividade'
            }]
          })(
            <Select>
              {
                clientData.activitiesData.map((activity, index) => (
                  <Option
                    value={activity.name}
                    key={index}
                  >
                    {activity.name}
                  </Option>
                ))
              }
            </Select>
          )}
        </Form.Item>
        <Form.Item
          label='Ano'
        >
          {getFieldDecorator('year', {
            initialValue: currentYear
          })(
            <Select>
              <Option value={currentYear - 1}>{currentYear - 1}</Option>
              <Option value={currentYear}>{currentYear}</Option>
              <Option value={currentYear + 1}>{currentYear + 1}</Option>
            </Select>
          )}
        </Form.Item>
        <Form.Item
          label='Mês'
        >
          {getFieldDecorator('month', {
            rules: [{
              required: true,
              message: 'Selecione o mês da mensalidade'
            }]
          })(
            <Select>
              {
                monthList.map((m, i) => (
                  <Option
                    key={i}
                    value={i + 1}
                  >
                    {m}
                  </Option>
                ))
              }
            </Select>
          )}
        </Form.Item>
        <Button type='primary' htmlType='submit'>
          Criar mensalidade
        </Button>
      </Form>
    );
  }
}
const CDebtForm = Form.create({
  name: 'create-debt-form'
})(DebtForm);

class CDebt extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientData: null,
      loading: false
    };
  }
  async getClientData(searchValue) {
    this.setState({
      loading: true
    });
    const nextState = {
      loading: false,
      clientData: null
    };
    try {
      const result = await client.getClientByRegistrationNumber(searchValue);
      if (!result) {
        throw new Error('Não foi possível encontrar o cliente');
      }
      nextState.clientData = result;
    } catch (e) {
      message.error(e.message);
    }
    this.setState(nextState);
  }
  resetData() {
    this.setState({
      loading: false,
      clientData: null
    });
  }
  render() {
    const { clientData, loading } = this.state;
    if (loading) {
      return <Spin />;
    }
    if (!clientData) {
      return <Search
        placeholder='Digite a matrícula do aluno para o qual deseja gerar a mensalidade'
        onSearch={(value, event) => this.getClientData(value, event)}
        style={{
          padding: '5px 0px 5px 0px'
        }}
      />;
    }
    return (
      <div>
        <Button
          type='default'
          onClick={() => this.resetData()}
          style={{
            margin: '10px 10px 10px 10px'
          }}
        >
          Voltar
        </Button>
        <div
          style={{
            margin: '0px 0px 20px 0px',
            padding: '10px',
            borderRadius: '10px',
            backgroundColor: 'rgba(0,0,0,.05)'
          }}
        >
          <Row gutter={16}>
            <Col span={8}>
              <Divider
                dashed
                orientation="left"
              >
                Cliente
            </Divider>
              <CClientCard {...clientData} />
            </Col>
            <Col span={8}>
              <Divider>
                Dados da Mensalidade
              </Divider>
              <CDebtForm {...{
                clientData,
                onRefresh: () => this.resetData()
              }} />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default CDebt;
