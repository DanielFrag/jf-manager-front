import React from 'react';
import { Spin, message, Row, Col, Input, Button, Divider, Modal } from 'antd';
import Client from '../controllers/client-controller';
import Payment from '../controllers/payment-controller';
import CDebtSummary from './debt-summary-component';
const { Search } = Input;
const client = new Client();
const payment = new Payment();
const { confirm } = Modal;
const formatter = new Intl.NumberFormat('pt-BR', {
  style: 'currency',
  currency: 'BRL',
  minimumFractionDigits: 2
});
class CHome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      selectedDebts: [],
      debtsMap: new Map()
    }
  }
  async getClientData(searchValue) {
    this.setState({
      loading: true
    });
    const nextState = {
      loading: false,
      clientData: null
    };
    try {
      const result = await client.getClientByRegistrationNumber(searchValue);
      if (!result) {
        throw new Error('Não foi possível encontrar o cliente');
      }
      nextState.clientData = result;
    } catch (e) {
      message.error(e.message);
    }
    this.setState(nextState);
  }
  getSpin() {
    return <Spin/>;
  }
  updatePaymentData() {
    const { debtsMap } = this.state;
    this.setState({
      selectedDebts: Array.from(debtsMap.values())
    });
  }
  selectDebt(debt) {
    const { debtsMap } = this.state;
    debtsMap.set(debt._id, debt);
    this.updatePaymentData();
  }
  removeDebt(debtId) {
    const { debtsMap } = this.state;
    debtsMap.delete(debtId);
    this.updatePaymentData();
  }
  getClientDebtsList(debtsArray, clientName) {
    const { debtsMap } = this.state;
    const inDebt = debtsArray.filter(d => !d.paymentId && d.valid);
    if (!inDebt.length) {
      message.success('Este aluno está em dia');
      return null;
    }
    const notSelectedDebt = inDebt
      .filter(d =>  !debtsMap.has(d._id))
      .map(d => ({
        valueFactor: 1,
        ...d
      }));
    notSelectedDebt.sort((a, b) => {
      return a.generated.date < b.generated.date ? -1 : 1;
    });
    return notSelectedDebt.map(d => (
      <div
        key={d._id}
      >
        <CDebtSummary
          debtData={{...d, clientName}}
        />
        <div
            style={{
              float: 'right',
              marginTop: '-68px',
              paddingRight: '10px'
            }}
          >
            <Button
              type='primary'
              icon='plus'
              size='large'
              shape='circle'
              onClick={() => this.selectDebt({...d, clientName})}
            />
          </div>
      </div>
    ));
  }
  updateSelectedDebt(debtData) {
    const { debtsMap } = this.state;
    const d = debtsMap.get(debtData._id);
    d.valueFactor = debtData.valueFactor;
    this.updatePaymentData();
  }
  getSelectedDebtList() {
    const { selectedDebts } = this.state;
    return selectedDebts.map(d => (
      <div
        key={d._id}
      >
        <CDebtSummary
          debtData={{...d}}
          canChange={true}
          onDebtChange={debtData => this.updateSelectedDebt(debtData)}
        />
        <div
            style={{
              float: 'right',
              marginTop: '-68px',
              paddingRight: '10px'
            }}
          >
            <Button
              type='danger'
              icon='minus'
              size='large'
              shape='circle'
              onClick={() => this.removeDebt(d._id)}
            />
          </div>
      </div>
    ));
  }
  resetData() {
    const { debtsMap } = this.state;
    debtsMap.clear();
    this.setState({
      selectedDebts: [],
      clientData: null
    });
  }
  tryPayment() {
    const { selectedDebts } = this.state;
    const value = selectedDebts.reduce((t, c) => t + (c.value * c.valueFactor), 0);
    const self = this;
    confirm({
      title: 'Confirmar pagamento',
      content: `Valor total: ${formatter.format(value)}`,
      async onOk() {
        try {
          await payment.create(selectedDebts);
          message.success('Pagamento cadastrado');
          self.resetData();
        } catch (e) {
          message.error(e.message);
        }
      },
      onCancel() {}
    });
  }
  getPaymentSummary() {
    const { selectedDebts } = this.state;
    const value = selectedDebts.reduce((t, c) => t + (c.value * c.valueFactor), 0);
    return (
      <div
        style={{
          textAlign: 'center'
        }}
      >
        <div>
          <h3>Total: {formatter.format(value)}</h3>
        </div>
        <div>
          <Button
            disabled={!value}
            block
            onClick={() => this.tryPayment()}
          >
            Realizar Pagamento
          </Button>
        </div>
      </div>
    );
  }
  render() {
    return (
      <Row>
        <Col
          span={12}
        >
          {
            this.state.loading ?
            this.getSpin() :
            <div
              style={{
                paddingRight: '10px'
              }}
            >
              <Divider
              >
                Buscar débitos de alunos
              </Divider>
              <Search
                placeholder='Digite a matrícula do aluno'
                onSearch={
                  value => this.getClientData(value)
                }
                style={{
                  padding: '5px 0px 5px 0px'
                }}
              />
              {
                this.state.clientData ?
                this.getClientDebtsList(this.state.clientData.debts, this.state.clientData.name) :
                null
              }
            </div>
          }
        </Col>
        <Col
          span={12}
        >
          <Divider
          >
            Dados do pagamento
          </Divider>
          {
            this.state.selectedDebts.length ?
            <div
              style={{
                paddingLeft: '10px'
              }}
            >
              {this.getSelectedDebtList()}
            </div> :
            <div
              style={{
                textAlign: 'center'
              }}
            >
              Nenhum débito selecionado
              <br/>
              <br/>
              <br/>
            </div>
          }
          {this.getPaymentSummary()}
        </Col>
      </Row>
    );
  }
}
export default CHome;
