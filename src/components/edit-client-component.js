import React from 'react';
import { Button, Spin, message, Row, Col, Divider, List } from 'antd';
import CClientCard from './client-data-component';
import Client from '../controllers/client-controller';
import CDrawer from './drawer-component';
import CClientPhotoUpload from './client-image-form';
import CEditClientData from './client-data-form';
import CClientActivityEditor from './client-activity-editor';
const client = new Client();
class CEditClient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      drawerVisible: false
    };
  }
  buildEditPanel() {
    let { clientData } = this.state;
    if (!clientData) {
      return null;
    }
    if (Array.isArray(clientData)) {
      return this.buildClientsList(clientData);
    }
    return (
      <div>
        <Row gutter={16}>
          <Col span={8}>
            <Divider
              dashed
              orientation="left"
            >
              Cliente
            </Divider>
            {this.getClientCard(clientData)}
          </Col>
          <Col span={8}>
            <Divider
              dashed
            >
              Foto
            </Divider>
            {this.getImageForm(clientData)}
            <Divider>Dados Cadastrais</Divider>
            {this.getClientDataEditForm(clientData)}
          </Col>
          <Col span={8}>
            <Divider
              dashed
            >
              Atividades
            </Divider>
            {this.getActivityEditorForm(clientData)}
          </Col>
        </Row>
      </div>
    );
  }
  getActivityEditorForm(clientData) {
    const props = {
      clientData,
      onRefresh: () => this.refreshClientData()
    };
    return <CClientActivityEditor {...props}/>;
  }
  getClientCard(clientData, selectable) {
    if (selectable) {
      clientData.expandAction = (registrationNumber) => this.getClientData(registrationNumber);
    }
    return <CClientCard {...clientData}/>
  }
  buildClientsList(clients) {
    return (
      <div>
        <List
          grid={{
            gutter: 16,
            column: 4
          }}
          dataSource={clients}
          renderItem={item => (
            <List.Item>
              {this.getClientCard(item, true)}
            </List.Item>
          )}
        />
      </div>
    );
  }
  async getClientData(searchValue) {
    this.setState({
      loading: true
    });
    const nextState = {
      loading: false,
      clientData: null
    };
    try {
      const result = await client.getClients(searchValue);
      if (!result.data || !result.data.length) {
        throw new Error('Não foi possível encontrar o cliente');
      }
      nextState.clientData = result.data.length === 1 ? result.data[0] : result.data;
    } catch (e) {
      message.error(e.message);
    }
    this.setState(nextState);
  }
  getImageForm(clientData) {
    const props = {
      clientData,
      onRefresh: () => this.refreshClientData()
    };
    return <CClientPhotoUpload {...props}></CClientPhotoUpload>
  }
  getClientDataEditForm(clientData) {
    const props = {
      clientData,
      onRefresh: () => this.refreshClientData()
    };
    return <CEditClientData {...props}/>;
  }
  getSpin() {
    return <Spin/>;
  }
  refreshClientData() {
    const { clientData } = this.state;
    if (!clientData || !clientData.registrationNumber) {
      return;
    }
    return this.getClientData(clientData.registrationNumber);
  }
  toggleDrawer() {
    this.setState({
      drawerVisible: !this.state.drawerVisible
    });
  }
  render() {
    if (this.state.loading) {
      return this.getSpin();
    }
    const { drawerVisible } = this.state;
    const onSearch = (value) => {
      this.toggleDrawer();
      this.getClientData(value);
    }
    const onClose = () => this.toggleDrawer();
    const drawerProps = {
      visible: drawerVisible,
      onSearch,
      onClose
    };
    return (
      <div>
        <Button
          type='primary'
          onClick={() => this.toggleDrawer()}
          style={{
            margin: '10px 10px 10px 10px'
          }}
        >
          Buscar aluno
        </Button>
        {this.buildEditPanel()}
        <CDrawer {...drawerProps}/>
      </div>
    );
  }
}
export default CEditClient;
