import React from 'react';
import { Button, Form, message, AutoComplete } from 'antd';
import Client from '../controllers/client-controller';
import Activity from '../controllers/activities-controller';
const client = new Client();
const activity = new Activity();
class ClientActivityForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addRequest: false,
      removeRequest: false,
      activitiesNamesList: [],
      dataSource: []
    };
    this.controllerFunc = {
      addRequest: client.addClientActivity,
      removeRequest: client.removeClientActivity,
      getActivitiesNames: activity.getActivitiesNamesList
    }
    this.getActivitiesNames();
  }
  async editClientActivity(e, actionName) {
    e.preventDefault();
    const { form, clientData, onRefresh } = this.props;
    let hasError;
    form.validateFields((err) => {
      if (err) {
        hasError = true;
      }
    });
    if (hasError) {
      return;
    }
    const formFields = form.getFieldsValue();
    const newState = {};
    newState[actionName] = true;
    this.setState({...newState});
    try {
      await this.controllerFunc[actionName](clientData._id, formFields.activityName);
    } catch (e) {
      message.error(e.message);
    }
    newState[actionName] = false;
    this.setState({...newState});
    form.resetFields();
    onRefresh();
  }
  async getActivitiesNames() {
    try {
      const list = await this.controllerFunc.getActivitiesNames();
      this.setState({
        activitiesNamesList: list,
        dataSource: list.slice()
      });
    } catch (e) {
      message.error(e.message);
      this.setState({
        activitiesNamesList: []
      });
    }
  }
  onSearch(value) {
    const { activitiesNamesList } = this.state;
    if (!value) {
      this.setState({
        dataSource: activitiesNamesList.slice()
      });
    }
    this.setState({
      dataSource: activitiesNamesList.filter(n => n.indexOf(value) !== -1)
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { addRequest, removeRequest, dataSource } = this.state;
    return (
      <Form>
        <Form.Item>
          {
            getFieldDecorator('activityName', {
              rules: [{
                required: true,
                message: 'Insira o nome da atividade'
              }]
            }) (
              <AutoComplete
                placeholder="Nome da atividade"
                onSearch={value => this.onSearch(value)}
                dataSource={dataSource}
              />
            )
          }
        </Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          icon="plus-circle"
          loading={addRequest}
          disabled={addRequest || removeRequest}
          onClick={(e) => this.editClientActivity(e, 'addRequest')}
        >
          Adicionar
        </Button>
        <Button
          type="danger"
          htmlType="submit"
          icon="minus-circle"
          loading={removeRequest}
          disabled={addRequest || removeRequest}
          style={{float: 'right'}}
          onClick={(e) => this.editClientActivity(e, 'removeRequest')}
        >
          Remover
        </Button>
      </Form>
    );
  }
}
const CClientActivityEditor = Form.create({
  name: 'clientActivityEdtor',
})(ClientActivityForm);
export default CClientActivityEditor;
