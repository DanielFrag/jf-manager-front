import React from 'react';
import { DatePicker, Spin, Button, message, Divider } from 'antd';
import PaymentRepo from '../repositories/payment-repository';
const formatter = new Intl.NumberFormat('pt-BR', {
  style: 'currency',
  currency: 'BRL',
  minimumFractionDigits: 2
});
const paymentRepo = new PaymentRepo();
class CPaymentReport extends React.Component {
  constructor(props) {
    super(props);
    const tomorrow = new Date();
    tomorrow.setUTCDate(tomorrow.getUTCDate() - 1);
    this.state = {
      startDate: tomorrow,
      loading: false
    };
  }
  disableEndDate(date) {
    const now = new Date();
    return date.valueOf() > new Date(now.getFullYear(), now.getMonth(), now.getDate());
  }
  setStartDate(date) {
    this.setState({
      startDate: new Date(date.format('YYYY-MM-DD')),
      momentStartDate: date
    });
  }
  async getPayments() {
    const { startDate, momentStartDate, currentPage } = this.state;
    const page = this.getButtonName() === 'Buscar' ? 1 : currentPage + 1
    this.setState({
      loading: true,
      previousSearch: momentStartDate
    });
    try {
      const data = await paymentRepo.getPayments(startDate.toISOString().substring(0, 10), page);
      this.setState({
        loading: false,
        data,
        currentPage: page
      });
    } catch (e) {
      message.error(`Erro ao buscar pagamentos: ${e.message}`);
      this.setState({
        loading: false
      });
    }
  }
  buildRegistrationDetail(debt, key, valueFactor) {
    return (
      <div
        key={key}
      >
        <Divider/>
        <div>
          <h4>
            Matrícula
          </h4>
          <span>
            Valor Cobrado: {formatter.format(debt.value * valueFactor)}
          </span>
          <br/>
          {
            valueFactor !== 1 ? <span>Valor Original: {formatter.format(debt.value)}</span> : null
          }
        </div>
        <div>
          Aluno: {debt.client.name} ({debt.client.registrationNumber})
        </div>
      </div>
    );
  }
  buildMonthlyPaymentDetail(debt, key, valueFactor) {
    return (
      <div
        key={key}
      >
        <Divider/>
        <div>
          <h4>
            Mensalidade {debt.activityName}
          </h4>
          <span>
            Ref: {debt.details.referenceMonth}
          </span>
          <br/>
          <span>
            Valor Cobrado: {formatter.format(debt.value * valueFactor)}
          </span>
          <br/>
          {
            valueFactor !== 1 ? <span>Valor Original: {formatter.format(debt.value)}</span> : null
          }
        </div>
        <div>
          Aluno: {debt.client.name} ({debt.client.registrationNumber})
        </div>
      </div>
    );
  }
  buildPaymentsList() {
    const { data } = this.state;
    if (!data || !data.length) {
      return null;
    }
    return (
      <div>
        {
          data.map((payment, index) => {
            const valueFactorMap = new Map(payment.debtsData.map(d => [d.id, d.valueFactor]));
            return (
              <div
                style={{
                  margin: '0px 0px 20px 0px',
                  padding: '10px',
                  borderRadius: '10px',
                  backgroundColor: 'rgba(0,0,0,.05)'
                }}
                key={index}
              >
                <div>
                  <h3>
                    {formatter.format(payment.total)}
                  </h3>
                  <div>
                    {new Date(payment.date).toLocaleDateString()}
                  </div>
                </div>
                {
                  payment.debts.map((d, i) => d.type === 'registration'
                    ? this.buildRegistrationDetail(d, i, valueFactorMap.get(d._id))
                    : this.buildMonthlyPaymentDetail(d, i, valueFactorMap.get(d._id)))
                }
              </div>
            );
          })
        }
      </div>
    );
  }
  getButtonName() {
    const { previousSearch, startDate, data } = this.state;
    const sameDate = previousSearch
      && previousSearch.format('YYYY-MM-DD') === startDate.toISOString().substring(0, 10);
    if (sameDate && data && data.length === 10) {
      return 'Mais';
    }
    return 'Buscar'
  }
  render() {
    const { loading, previousSearch } = this.state;
    if (loading) {
      return <Spin />;
    }
    return (
      <div>
        <div>
          <DatePicker
            defaultValue={previousSearch}
            disabledDate={date => this.disableEndDate(date)}
            format='DD/MM/YYYY'
            onChange={(date, dateStr) => this.setStartDate(date, dateStr)}
          ></DatePicker>
        </div>
        <div>
          <Button
            type={this.getButtonName() === 'Buscar' ? 'primary' : 'default'}
            onClick={() => this.getPayments()}
            style={{
              margin: '10px 10px 10px 10px'
            }}
            disabled={loading}
          >
            {this.getButtonName()}
          </Button>
        </div>
        <div>
          {this.buildPaymentsList()}
        </div>
      </div>
    );
  }
}
export default CPaymentReport;
