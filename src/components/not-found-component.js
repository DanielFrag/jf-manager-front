import React from 'react';
import { Redirect } from 'react-router-dom';
class CNotFound extends React.Component {
  render() {
    return <Redirect to={{ pathname: "/" }} />;
  }
}
export default CNotFound;
