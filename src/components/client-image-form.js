import React from 'react';
import { Upload, Icon, Button, message } from 'antd';
import Client from '../controllers/client-controller';
const client = new Client();
class CClientPhotoUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientData: props.clientData,
      onRefresh: props.onRefresh,
      file: null,
      uploading: false
    }
  }
  checkFileData(file) {
    this.setState({
      file
    });
    return false;
  }
  async uploadPhoto() {
    const { file, clientData, onRefresh } = this.state;
    try {
      await client.updateClientPhoto(clientData, file);
      message.success('Cliente atualizado com sucesso');
    } catch (e) {
      message.error(e.message);
    }
    this.setState({
      file: null,
      uploading: false
    });
    onRefresh();
  }
  render() {
    const { uploading, file } = this.state;
    return(
      <div>
        <div>
          <Upload
            name='file'
            beforeUpload={file => this.checkFileData(file)}
            showUploadList={false}
          >
            <Button>
              <Icon type="upload" /> {file ? file.name : 'Escolha uma foto (png)'}
            </Button>
          </Upload>
        </div>
        <div>
          <Button
            type="primary"
            onClick={() => this.uploadPhoto()}
            disabled={!file}
            loading={uploading}
            style={{ marginTop: 16 }}
          >
            {uploading ? 'Uploading' : 'Enviar foto'}
          </Button>
        </div>
      </div>
    );
  }
}
export default CClientPhotoUpload;
