import React from 'react';
import { Button, Form, Icon, Input, message } from 'antd';
import AuthorizationService from '../services/authorization-service';
import { Redirect } from 'react-router-dom';
const authorizationService = new AuthorizationService();
class CAuthorizationForm extends React.Component {
  handleSubmit(e) {
    e.preventDefault();
    const { form } = this.props;
    let hasError;
    form.validateFieldsAndScroll((err) => {
      if (err) {
        hasError = true;
      }
    });
    if (hasError) {
      return;
    }
    const formFields = form.getFieldsValue();
    this.setAuthData(formFields.userName, formFields.password);
    message.success('Dados de autenticação gravados com sucesso');
    form.resetFields();
  }
  setAuthData(user, pass) {
    authorizationService.setAuthorizationData(user, pass);
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    if (authorizationService.isLogged()) {
      return <Redirect to='/app/home'/>;
    }
    return (
      <Form
        style={{maxWidth: '300px'}}
        onSubmit={(e) => this.handleSubmit(e)}
      >
        <Form.Item>
          {
            getFieldDecorator('userName', {
              rules: [{
                required: true,
                message: 'Insira o nome de usuário'
              }]
            })(
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Usuário"
              >
              </Input>
            )
          }
        </Form.Item>
        <Form.Item>
          {
            getFieldDecorator('password', {
              rules: [{
                required: true,
                message: 'Insira a senha'
              }],
            })(
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                type="password"
                placeholder="Senha"
              />,
            )
          }
        </Form.Item>
        <Button
          type="primary"
          htmlType="submit"
        >
          Ok
        </Button>
      </Form>
    );
  }
}
const CAuthorization = Form.create({
  name: 'authorizationForm'
})(CAuthorizationForm);
export default CAuthorization;
