import React from 'react';
import { Form, Input, Button, Modal, message, Select } from 'antd';
import Client from '../controllers/client-controller';
import validateCpf from '../utils/cpf-validator';
const { Option } = Select;
const client = new Client();
const { confirm } = Modal;
class EditClientForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientData: props.clientData,
      onRefresh: props.onRefresh
    }
  }
  confirmData() {
    const { form } = this.props;
    const { clientData, onRefresh } = this.state;
    let hasError;
    form.validateFieldsAndScroll((err) => {
      if (err) {
        hasError = true;
      }
    });
    if (hasError) {
      return;
    }
    const formFields = form.getFieldsValue();
    confirm({
      title: 'Deseja realmente atualizar os dados cadastrais do aluno?',
      async onOk() {
        try {
          await client.updateClientData(clientData._id, formFields);
          message.success('Atualizado com sucesso');
          form.resetFields();
          onRefresh();
        } catch (e) {
          message.error(e.message);
        }
      },
      onCancel() { }
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    this.confirmData();
  }
  checkCpf(rule, value, cb) {
    if (!value) {
      return cb();
    }
    const isValid = validateCpf(value);
    if (isValid) {
      return cb();
    }
    cb('cpf inválido');
  }
  disableEndDate(date) {
    const now = new Date();
    return date.valueOf() > new Date(now.getFullYear(), now.getMonth(), now.getDate());
  }
  checkPhone(rule, value, cb) {
    if (!value) {
      return cb();
    }
    const phoneRegex = /^[0-9]{8,11}$/;
    if (!phoneRegex.test(value)) {
      return cb('Telefone inválido');
    }
    cb();
  }
  checkDueDate(rule, value, cb) {
    if (!value) {
      return cb();
    }
    const integerValue = parseInt(value, 10);
    if (!integerValue || integerValue < 1 || integerValue > 28) {
      return cb('Dia para vencimento inválido');
    }
    cb();
  }
  // onChangeClientStatus(status) {
  //   this.setState({
  //     inactive: checked
  //   });
  // }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { clientData } = this.state;
    return (
      <Form onSubmit={(e) => this.handleSubmit(e)}>
        <Form.Item
          label='CPF'
        >
          {getFieldDecorator('cpf', {
            rules: [{
              validator: this.checkCpf
            }]
          })(
            <Input
              placeholder='Apenas números'
            />
          )}
        </Form.Item>
        <Form.Item
          label='Telefone'
        >
          {getFieldDecorator('phone', {
            rules: [{
              validator: this.checkPhone
            }]
          })(
            <Input
              placeholder='Apenas números'
            />
          )}
        </Form.Item>
        <Form.Item
          label='Dia de vencimento para mensalidades'
        >
          {getFieldDecorator('debtsExpireDate', {
            rules: [{
              validator: this.checkDueDate
            }]
          })(
            <Input
              placeholder='Escolha um dia do mês entre 1 e 28'
            />
          )}
        </Form.Item>
        <Form.Item
          label='Status do aluno'
        >
          {getFieldDecorator('status', {
            initialValue: clientData.status
          })(
            <Select>
              <Option value="locked">Matrícula trancada</Option>
              <Option value="active">Ativo</Option>
            </Select>
          )}
        </Form.Item>
        <Button type='primary' htmlType='submit'>
          Enviar dados
        </Button>
      </Form>
    );
  }
}
const CEditClientData = Form.create({
  name: 'clientEditData'
})(EditClientForm);
export default CEditClientData;
