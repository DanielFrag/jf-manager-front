import React from 'react';
import { Badge, Descriptions, Icon, InputNumber } from 'antd';
const formatter = new Intl.NumberFormat('pt-BR', {
  style: 'currency',
  currency: 'BRL',
  minimumFractionDigits: 2
});
class CDebtSummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      valueFactor: props.valueFactor
    };
  }
  getFormatedDueDate(dateStr) {
    const d = new Date(dateStr);
    return `${
      d.getUTCDate().toString().padStart(2, '0')
    }/${
      (1 + d.getUTCMonth()).toString().padStart(2, '0')
    }/${
      d.getUTCFullYear()
    }`;
  }
  getFormatedReferenceMonth(refMonth) {
    return `${refMonth.substring(5, 8)}/${refMonth.substring(0, 4)}`;
  }
  getTitle(debtData) {
    if (debtData.type === 'registration') {
      return 'Matrícula';
    }
    return `Mensalidade ${this.getFormatedReferenceMonth(debtData.details.referenceMonth)}`;
  }
  render() {
    const  { debtData, canChange, onDebtChange } = this.props;
    return (
      <div
        style={{
          margin: '0px 0px 20px 0px',
          padding: '10px',
          borderRadius: '10px',
          backgroundColor: 'rgba(0,0,0,.05)'
        }}
      >
        {
          new Date() > new Date(debtData.details.dueDate) ?
            <Icon
              type="exclamation-circle"
              style={{
                color: '#f5222d',
                fontSize: '40px',
                float: 'right'
              }}
              theme='filled'
            /> :
            null
        }
        <Descriptions
          title={this.getTitle(debtData)}
          size='small'
        >
          {
            debtData.clientName ?
            <Descriptions.Item 
              label='Aluno(a)'
              span={3}
            >
              {debtData.clientName}
            </Descriptions.Item> :
            null
          }
          {
            debtData.activityName ?
            <Descriptions.Item
              label='Aula'
              span={3}
            >
              {debtData.activityName}
            </Descriptions.Item> :
            null
          }
          <Descriptions.Item
            label='Vencimento'
          >
            {
              new Date() > new Date(debtData.details.dueDate) ?
              <Badge
                status='error'
              >
              </Badge> :
              null
            }
            {
              this.getFormatedDueDate(debtData.details.dueDate)
            }
          </Descriptions.Item>
          <Descriptions.Item label='Valor'>
            {formatter.format(debtData.value)}
          </Descriptions.Item>
          {
            !canChange ? null : <Descriptions.Item label='Taxa(%)'>
              <InputNumber
                min={-100}
                max={100}
                disabled={debtData.inputFreeze}
                onChange={value => onDebtChange({
                  ...debtData,
                  valueFactor: value ? 1 + ((parseFloat(value) || 0) / 100) : 1
                })}
              >
              </InputNumber>
            </Descriptions.Item>
          }
          {
            debtData.valueFactor === 1 || typeof debtData.valueFactor !== 'number' ? null : <Descriptions.Item label='Valor com taxas'>
              {formatter.format(debtData.value * debtData.valueFactor)}
            </Descriptions.Item>
          }
        </Descriptions>
      </div>
    );
  }
}
export default CDebtSummary;
