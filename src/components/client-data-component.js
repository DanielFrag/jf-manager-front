import React from 'react';
import { Card, Empty } from 'antd';
const { Meta } = Card;
class CClientCard extends React.Component {
  getImage() {
    if (this.props.photo) {
      return <img alt='' src={`data:image/png;base64, ${this.props.photo}`}/>;
    }
    return <img alt='' src={Empty.PRESENTED_IMAGE_SIMPLE}/>;
  }
  formatBirthDate(birthDate) {
    return `${birthDate.substr(8, 2)}/${birthDate.substr(5, 2)}/${birthDate.substr(0, 4)}`
  }
  formatActivities(activitiesData) {
    return (
      <ul>
        {activitiesData.map(a => <li key={a.name}>{a.name}</li>)}
      </ul>
    );
  }
  render() {
    const {
      birthDate,
      cpf,
      name,
      phone,
      registrationNumber,
      activitiesData,
      debtsExpireDate,
      expandAction
    } = this.props;
    return (
      <Card
        style={{
          width: 240
        }}
        cover={this.getImage()}
        hoverable={!!expandAction}
        onClick={expandAction ? () => expandAction(registrationNumber) : null}
      >
        <Meta
          title={name}
          description={registrationNumber}
        />
        <br/>
        {birthDate ? <p>Data de nascimento: {this.formatBirthDate(birthDate)}</p> : null}
        {cpf ? <p>CPF: {cpf}</p>: null}
        {phone ? <p>Telefone: {phone}</p>: null}
        {activitiesData.length ? this.formatActivities(activitiesData) : null}
        {debtsExpireDate ? <p>Vencimento (dia): {debtsExpireDate}</p> : null}
      </Card>
    );
  }
}
export default CClientCard;
