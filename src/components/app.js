import React from 'react';
import { Layout } from 'antd';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import CMenu from './menu-component';
import CHome from './home-component';
import CEditClient from './edit-client-component';
import CNewClient from './new-client-component';
import CPaymentsReport from './payment-report-component';
import CDebt from './debt-component';
// import CEditActivity from '../components/edit-activity-component';
import CPayments from './payments-component';
import AuthorizationService from '../services/authorization-service';
import '../index.css';
const authorization = new AuthorizationService();
const {
  Header, Content, Footer
} = Layout;
class Main extends React.Component {
  render() {
    if (!authorization.isLogged()) {
      return <Redirect to='/'/>;
    }
    return (
      <Layout>
        <BrowserRouter>
          <Header
            style={{
              position: 'fixed',
              zIndex: 1,
              width: '100%'
            }}
          >
            <CMenu/>
          </Header>
          <Content
            style={{
              padding: '50px 50px 0px 50px',
              marginTop: 64,
              minHeight: `${window.innerHeight - 64 - 69}px`
            }}
          >
            <Switch>
              <Route path="/app" exact={true} component={CHome} />
              <Route path="/app/home" exact={true} component={CHome} />
              <Route path="/app/edit-client" exact={true} component={CEditClient} />
              <Route path="/app/new-client" exact={true} component={CNewClient} />
              <Route path="/app/payments" exact={true} component={CPayments} />
              <Route path="/app/payments-report" exact={true} component={CPaymentsReport} />
              <Route path="/app/debts" exact={true} component={CDebt} />
            </Switch>
          </Content>
          <Footer
            style={{
              textAlign: 'center'
            }}
          >
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </BrowserRouter>
      </Layout>
    );
  }
}
export default Main;
