import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import AuthorizationService from '../services/authorization-service';
import '../index.css';
import { Menu, Button } from 'antd';
const CMenu = withRouter(props => <CustomMenu {...props}/>);
const authorization = new AuthorizationService();
class CustomMenu extends React.Component {
  render() {
    return (
      <Menu
        theme="dark"
        mode="horizontal"
        selectedKeys={[this.props.location.pathname]}
        style={{
          lineHeight: '64px'
        }}
      >
        <Menu.Item key="/home">
          <Link to="/app/home">
            Home
          </Link>
        </Menu.Item>
        <Menu.Item key="/new-client">
          <Link to="/app/new-client">
            Novo Aluno
          </Link>
        </Menu.Item>
        <Menu.Item key="/edit-client">
          <Link to="/app/edit-client">
            Editar Dados do Aluno
          </Link>
        </Menu.Item>
        <Menu.Item key="/payments">
          <Link to="/app/payments">
            Pagamentos
          </Link>
        </Menu.Item>
        <Menu.Item key="/authorization" style={{float: 'right'}}>
          <Button to="/" onClick={() => {
            authorization.logout();
            window.location = '/'
          }}>
            Logout
          </Button>
        </Menu.Item>
        <Menu.Item key="/paymets-report">
          <Link to="/app/payments-report">
            Caixa
          </Link>
        </Menu.Item>
        <Menu.Item key="/debts">
          <Link to="/app/debts">
            Mensalidades
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}
export default CMenu;
