import React from 'react';
import {
  Form, Input, DatePicker, Button, Modal, message
} from 'antd';
import Client from '../controllers/client-controller';
import validateCpf from '../utils/cpf-validator';
const client = new Client();
const { confirm } = Modal;
class NewClientForm extends React.Component {
  confirmData() {
    const { form } = this.props;
    let hasError;
    form.validateFieldsAndScroll((err) => {
      if (err) {
        hasError = true;
      }
    });
    if (hasError) {
      return;
    }
    const formFields = form.getFieldsValue();
    confirm({
      title: 'Deseja realmente cadastrar o aluno:',
      content: `${formFields.name}`,
      async onOk() {
        try {
          await client.create(formFields);
          message.success('Cadastrado com sucesso');
          form.resetFields();
        } catch (e) {
          message.error(e.message);
        }
      },
      onCancel() {}
    })
  }
  handleSubmit(e) {
    e.preventDefault();
    this.confirmData();
  }
  checkCpf(rule, value, cb) {
    if (!value) {
      return cb();
    }
    const isValid = validateCpf(value);
    if (isValid) {
      return cb();
    }
    cb('cpf inválido');
  }
  disableEndDate(date) {
    const now = new Date();
    return date.valueOf() > new Date(now.getFullYear(), now.getMonth(), now.getDate());
  }
  checkPhone(rule, value, cb) {
    if (!value) {
      return cb();
    }
    const phoneRegex = /^[0-9]{8,11}$/;
    if (!phoneRegex.test(value)) {
      return cb('Telefone inválido');
    }
    cb();
  }
  checkDueDate(rule, value, cb) {
    if (!value) {
      return cb();
    }
    const integerValue = parseInt(value, 10);
    if (!integerValue || integerValue < 1 || integerValue > 28) {
      return cb('Dia para vencimento inválido');
    }
    cb();
  }
  render() {
    const {getFieldDecorator} = this.props.form;
    return (
      <Form onSubmit={(e) => this.handleSubmit(e)}>
        <Form.Item
          label='Nome do aluno'
        >
          {getFieldDecorator('name', {
            rules: [{
              required: true,
              message: 'Nome obrigatório',
            }],
          })(
            <Input/>
          )}
        </Form.Item>
        <Form.Item
          label='Data de nasciemnto'
        >
          {getFieldDecorator('birthDate', {
            rules: [{
              required: true,
              message: 'Data de nascimento obrigatória',
            }],
          })(
            <DatePicker
              disabledDate={this.disableEndDate}
              format='DD/MM/YYYY'
            ></DatePicker>
          )}
        </Form.Item>
        <Form.Item
          style={{width: '342px'}}
          label='CPF'
        >
          {getFieldDecorator('cpf', {
            rules: [{
              validator: this.checkCpf
            }]
          })(
            <Input
              placeholder='Apenas números'
            />
          )}
        </Form.Item>
        <Form.Item
          style={{width: '342px'}}
          label='Telefone'
        >
          {getFieldDecorator('phone', {
            rules: [{
              validator: this.checkPhone
            }]
          })(
            <Input
              placeholder='Apenas números'
            />
          )}
        </Form.Item>
        <Form.Item
          style={{width: '342px'}}
          label='Dia de vencimento para mensalidades'
        >
          {getFieldDecorator('debtsExpireDate', {
            rules: [{
              validator: this.checkDueDate
            }]
          })(
            <Input
              placeholder='Escolha um dia do mês entre 1 e 28'
            />
          )}
        </Form.Item>
        <Button type='primary' htmlType='submit'>
          Cadastrar Aluno
        </Button>
      </Form>
    );
  }
}
const CNewClient = Form.create({
  name: 'clientRegister'
})(NewClientForm);
export default CNewClient;
