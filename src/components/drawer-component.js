import React from 'react';
import { Input, Drawer } from 'antd';
const { Search } = Input;
class CDrawer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible
    }
  }
  componentWillReceiveProps(props) {
    this.setState({
      visible: props.visible
    });
  }
  render() {
    return(
      <Drawer
        placement='top'
        visible={this.state.visible}
        closable={false}
        onClose={() => this.props.onClose()}
      >
        <Search
          placeholder='Digite a matrícula ou o nome do aluno'
          onSearch={value => this.props.onSearch(value)}
          style={{
            padding: '5px 5px 5px 5px'
          }}
        />
      </Drawer>
    );
  }
}
export default CDrawer;
