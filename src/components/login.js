import React from 'react';
import { Layout } from 'antd';
import { Redirect } from 'react-router-dom';
import CAuthorization from '../components/authorization-component';
import AuthorizationService from '../services/authorization-service';
import '../index.css';
const authorization = new AuthorizationService();
const {
  Header, Content, Footer
} = Layout;
class Main extends React.Component {
  render() {
    if (authorization.isLogged()) {
      return <Redirect to='/app'/>;
    }
    return (
      <Layout>
        <Header
          style={{
            position: 'fixed',
            zIndex: 1,
            width: '100%'
          }}
        >
        </Header>
        <Content
          style={{
            padding: '50px 50px 0px 50px',
            marginTop: 64,
            minHeight: `${window.innerHeight - 64 - 69}px`
          }}
        >
          <CAuthorization/>
        </Content>
        <Footer
          style={{
            textAlign: 'center'
          }}
        >
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    );
  }
}
export default Main;
