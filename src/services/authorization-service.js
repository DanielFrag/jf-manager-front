class AuthorizationService {
  setAuthorizationData(user, pass) {
    window.sessionStorage.setItem('user:pass', btoa(`${user}:${pass}`));
  }
  getAuthorizationData() {
    return `Basic ${window.sessionStorage.getItem('user:pass')}`;
  }
  isLogged() {
    return !!window.sessionStorage.getItem('user:pass');
  }
  logout() {
    window.sessionStorage.setItem('user:pass', '');
  }
}
export default AuthorizationService;
