import ClientRepository from '../repositories/client-repository';
const clientRepository = new ClientRepository();
class Client {
  create(formData) {
    const name = formData.name.split(' ').filter(s => s).join(' ');
    const data = {
      name,
      birthDate: formData.birthDate.format('YYYY-MM-DD')
    };
    if (formData.phone) {
      data.phone = formData.phone;
    }
    if (formData.cpf) {
      data.cpf = formData.cpf;
    }
    if (formData.debtsExpireDate) {
      data.debtsExpireDate = parseInt(formData.debtsExpireDate, 10);
    }
    return clientRepository.createOne(data);
  }
  getQueryData(searchValue, page) {
    const registrationNumberRegex = /^[0-9]{8}$/;
    if (registrationNumberRegex.test(searchValue)) {
      return {
        registrationNumber: searchValue
      };
    }
    return {
      name: searchValue,
      page: page || 1
    };
  }
  getClients(searchValue, page) {
    return clientRepository.readClientsCompleteData(this.getQueryData(searchValue, page));
  }
  async getClientByRegistrationNumber(registrationNumber) {
    const queryData = this.getQueryData(registrationNumber);
    if (!queryData.registrationNumber) {
      throw new Error('Matrícula inválida');
    }
    const result = await clientRepository.readClientsCompleteData(queryData);
    return result && result.data && result.data[0];
  }
  updateClientPhoto(clientData, file) {
    if (!clientData || !clientData._id) {
      throw new Error('Cliente inválido');
    }
    if (!file || file.type !== 'image/png') {
      throw new Error('Selecione um arquivo válido (imagem/png)');
    }
    return clientRepository.updateOneImage(clientData._id, file);
  }
  updateClientData(clientId, clientData) {
    const updateData = {};
    ['cpf', 'phone', 'debtsExpireDate'].forEach(field => {
      if (clientData[field]) {
        updateData[field] = clientData[field];
      }
    });
    if (clientData.debtsExpireDate) {
      updateData.debtsExpireDate = parseInt(clientData.debtsExpireDate);
    }
    if (clientData.status) {
      updateData.status = clientData.status;
    }
    if (!Object.keys(updateData).length) {
      throw new Error('Dados inválidos');
    }
    return clientRepository.updateOneData(clientId, updateData);
  }
  addClientActivity(clientId, activityName) {
    return clientRepository.updateOneAddActivity(clientId, {
      name: activityName
    });
  }
  removeClientActivity(clientId, activityName) {
    return clientRepository.updateOneRemoveActivity(clientId, activityName);
  }
}
export default Client;
