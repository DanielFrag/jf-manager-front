import ActivityRepository from '../repositories/activities-repository';
const activityRepository = new ActivityRepository();
class Activity {
  getActivitiesNamesList() {
    return activityRepository.getActivitiesNames();
  }
}
export default Activity;
