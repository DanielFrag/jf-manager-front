import DebtsRepository from '../repositories/debts-repository';
const debtsRepository = new DebtsRepository();
class Payment {
  create(clientData, debtData) {
    const refMonth = `${debtData.year}-${debtData.month.toString().padStart(2, '0')}`
    return debtsRepository.createOne(clientData._id, refMonth, debtData.activity);
  }
}
export default Payment;
