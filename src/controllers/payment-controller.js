import PaymentRepository from '../repositories/payment-repository';
const paymentRepository = new PaymentRepository();
class Payment {
  create(debts) {
    const idsArray = debts.map((d) => {
      const debtData = {
        id: d._id
      }
      if (d.valueFactor) {
        debtData.valueFactor = d.valueFactor;
      }
      return debtData;
    });
    return paymentRepository.createOne(idsArray);
  }
}
export default Payment;
