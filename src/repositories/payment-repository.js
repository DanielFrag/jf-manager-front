import request from  '../utils/request';
import AuthorizationService from '../services/authorization-service';
import config from '../config/config';
const authorizationService = new AuthorizationService();
class PaymentRepository {
  constructor() {
    this.apiUrl = `${config.API_URL}/payments`;
  }
  checkUnauthorized(status) {
    if (status === 401) {
      throw new Error('Ação não aturorizada. Verifique os dados de autenticação');
    }
  }
  async createOne(data) {
    const result = await request({
      url: this.apiUrl,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      }
    }, data);
    this.checkUnauthorized(result.status);
    if (result.status !== 201) {
      const unknow = 'desconhecido';
      throw new Error(
        `Erro ao criar pagamento. Código: ${
          result.status || unknow
        }. Detalhes: ${JSON.stringify(result.data) || unknow}`
      );
    }
  }
  async getPayments(startDate, page, pagination) {
    const qs = {};
    if (startDate) {
      qs.startDate = startDate;
    }
    if (page) {
      qs.page = page;
    }
    if (pagination) {
      qs.pagination = pagination;
    }
    const result = await request({
      url: this.apiUrl,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      },
      qs
    });
    this.checkUnauthorized(result.status);
    if (result.status !== 200) {
      throw new Error(result.statusText);
    }
    return result.data;
  }
}
export default PaymentRepository;
