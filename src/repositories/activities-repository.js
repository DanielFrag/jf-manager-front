import request from  '../utils/request';
import AuthorizationService from '../services/authorization-service';
import config from '../config/config';
const authorizationService = new AuthorizationService();
class ActivityRepository {
  constructor() {
    this.apiUrl = `${config.API_URL}/activities`;
  }
  checkUnauthorized(status) {
    if (status === 401) {
      throw new Error('Ação não aturorizada. Verifique os dados de autenticação');
    }
  }
  async getActivitiesNames() {
    // get /api/activities?namesOnly=true
    const qs = {
      namesOnly: true
    };
    const result = await request({
      url: this.apiUrl,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      },
      qs
    });
    this.checkUnauthorized(result.status);
    return result.data;
  }
}
export default ActivityRepository;
