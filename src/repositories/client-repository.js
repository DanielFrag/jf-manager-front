import request from  '../utils/request';
import AuthorizationService from '../services/authorization-service';
import config from '../config/config';
const authorizationService = new AuthorizationService();
class ClientRepository {
  constructor() {
    this.apiUrl = `${config.API_URL}/clients`;
  }
  checkUnauthorized(status) {
    if (status === 401) {
      throw new Error('Ação não aturorizada. Verifique os dados de autenticação');
    }
  }
  async createOne(data) {
    const result = await request({
      url: this.apiUrl,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      }
    }, data);
    this.checkUnauthorized(result.status);
    if (result.status !== 201) {
      const unknow = 'desconhecido';
      throw new Error(
        `Não foi possível cadastrar o aluno. Erro: ${
          result.status || unknow
        }. Detalhes: ${JSON.stringify(result.data) || unknow}`
      );
    }
  }
  async updateOneAddActivity(clientId, activityData) {
    // patch /api/clients/:clientId/activities
    const result = await request({
      url: `${this.apiUrl}/${clientId}/activities`,
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      }
    }, activityData);
    this.checkUnauthorized(result.status);
    if (result.status !== 204) {
      throw new Error('Não foi possível adicionar a atividade a este cliente');
    }
  }
  async updateOneData(clientId, updateData) {
    // patch /api/clients/:clientId
    const result = await request({
      url: `${this.apiUrl}/${clientId}`,
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      }
    }, updateData);
    this.checkUnauthorized(result.status);
    if (result.status !== 204) {
      throw new Error(result.statusText);
    }
    return result;
  }
  async updateOneImage(clientId, photo) {
    const result = await request({
      url: `${this.apiUrl}/${clientId}/image`,
      method: 'PATCH',
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': authorizationService.getAuthorizationData()
      }
    }, {
      photo
    });
    this.checkUnauthorized(result.status);
    if (result.status !== 204) {
      throw new Error('result.statusText');
    }
    return result;
  }
  async updateOneRemoveActivity(clientId, activityName) {
    // delete /api/clients/:clientId/activities/:activityName
    const result = await request({
      url: `${this.apiUrl}/${clientId}/activities/${activityName}`,
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      }
    });
    this.checkUnauthorized(result.status);
    if (result.status !== 204) {
      throw new Error('Não foi possível remover esta atividade deste cliente');
    }
  }
  async readOne() {
    // get /api/clients/:clientId
    return Promise.resolve();
  }
  async readClientsCompleteData(queryString) {
    // get /api/clients?registrationNumber=clientRegistrationNumber
    // get /api/clients?clientName=clientName
    queryString.sort = 'name';
    const result = await request({
      url: this.apiUrl,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      },
      qs: queryString
    });
    this.checkUnauthorized(result.status);
    return result;
  }
  async readMany() {
    // get /api/clients?page=10&pagination=4
    return Promise.resolve();
  }
}
export default ClientRepository;
