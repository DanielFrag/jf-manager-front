import request from  '../utils/request';
import AuthorizationService from '../services/authorization-service';
import config from '../config/config';
const authorizationService = new AuthorizationService();
class DebtsRepository {
  constructor() {
    this.apiUrl = `${config.API_URL}/debts`;
  }
  checkUnauthorized(status) {
    if (status === 401) {
      throw new Error('Ação não aturorizada. Verifique os dados de autenticação');
    }
  }
  async createOne(clientId, referenceMonth, activityName) {
    const result = await request({
      url: this.apiUrl,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authorizationService.getAuthorizationData()
      }
    }, {
      clientId,
      referenceMonth,
      activityName
    });
    this.checkUnauthorized(result.status);
    if (result.status !== 201) {
      let detail;
      switch (result.status) {
        case (409):
          detail = 'este débito já existe';
          break;
        case (412):
          detail = 'condição não atendida';
          break;
        default:
          detail = 'desconhecido';
      }
      throw new Error(`Erro ao criar débito. Detalhes: ${detail}`);
    }
  }
}
export default DebtsRepository;
