const contentTypeBodyHandler = new Map();
contentTypeBodyHandler.set('application/json', (data) => {
  if (typeof data === 'object') {
    return JSON.stringify(data);
  }
  return typeof data === 'string' ? data : '{}';
});
contentTypeBodyHandler.set('multipart/form-data', (data) => {
  if (data instanceof FormData) {
    return data;
  }
  const form = new FormData();
  if (typeof data === 'object') {
    Object
      .entries(data)
      .forEach(([key, value]) => {
        form.append(key, value)
      });
  }
  return form;
});
const handleRequestBody = (contentType, data) => {
  if (!data || typeof data === 'string') {
    return data;
  }
  const bodyHandler = contentTypeBodyHandler.get(contentType);
  if (bodyHandler) {
    return bodyHandler(data);
  }
};
const buildValidationResult = (isValid, message) => {
  const result = {
    valid: isValid
  };
  if (message) {
    result.message = message;
  }
  return result;
}
const handleQueryStringPrimitiveValue = (value) => {
  if (typeof value === 'number' || typeof value === 'boolean') {
    return `${value}`;
  }
  return `${value.split(' ').join('+')}`;
};
const buildUrl = (host, qs) => {
  if (!qs) {
    return host;
  }
  const qsResult = Object
    .entries(qs)
    .map(([key, value]) => {
      if (typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean') {
        return `${key}=${handleQueryStringPrimitiveValue(value)}`;
      }
      return `${key}=${value.map(v => handleQueryStringPrimitiveValue(v)).join(',')}`;
    })
    .join('&');
  return `${host}?${qsResult}`;
};
const validateOptions = (options) => {
  if (!options || typeof options !== 'object') {
    return buildValidationResult(false, 'Options is not defined');
  }
  const { url, method, headers, qs } = options;
  if (!url || typeof url !== 'string') {
    return buildValidationResult(false, 'Url is not defined');
  }
  if (!method || ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'].every(m => method !== m)) {
    return buildValidationResult(false, 'Method is not defined');
  }
  if (!headers || typeof headers !== 'object' || Object.values(headers).some(h => typeof h !== 'string')) {
    return buildValidationResult(false, 'Invalid headers object');
  }
  if (qs) {
    if (typeof qs !== 'object') {
      return buildValidationResult(false, 'Invalid query string object');
    }
    const qsInvalidData = Object
      .values(qs)
      .some((q) => {
        if (typeof q === 'string' || typeof q === 'number' || typeof q === 'boolean') {
          return false;
        }
        return !(Array.isArray(q) 
          && q.some(arrayItem => typeof arrayItem === 'string' || typeof arrayItem === 'number'));
      });
    if (qsInvalidData) {
      return buildValidationResult(false, 'Invalid query string data.');
    }
  }
  return buildValidationResult(true);
};
const request = (options, data) => {
  const validOptions = validateOptions(options);
  if (!validOptions.valid) {
    return Promise.reject(validOptions.message);
  }
  const request = new XMLHttpRequest();
  request.open(options.method, buildUrl(options.url, options.qs));
  Object
    .entries(options.headers)
    .forEach(([key, value]) => {
      //TODO gambiarra
      if (value !== 'multipart/form-data') {
        request.setRequestHeader(key, value);
      }
    });
  request.responseType = 'json';
  const returnedPromise = new Promise((resolve, reject) => {
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        return resolve({
          status: request.status,
          data: request.response,
          statusText: request.statusText
        })
      }
    };
  });
  let requestBody = handleRequestBody(options.headers['Content-Type'], data);
  request.send(requestBody);
  return returnedPromise;
};
export default request;
