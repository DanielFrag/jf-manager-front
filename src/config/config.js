export default {
  API_URL: process.env.NODE_ENV === 'production' ? 'https://jf-manager-api.herokuapp.com/api' : 'http://localhost:8080/api'
};
