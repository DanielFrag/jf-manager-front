import React from 'react';
import { Layout } from 'antd';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import App from '../components/app'
import CNotFound from '../components/not-found-component';
import Login from '../components/login';
class Main extends React.Component {
  render() {
    return (
      <Layout>
        <BrowserRouter>
          <Switch>
            <Route path="/app" exact={true} component={App} />
            <Route path="/" exact={true} component={Login} />
            <Route path='*' component={CNotFound} />
          </Switch>
        </BrowserRouter>
      </Layout>
    );
  }
}
export default Main;
